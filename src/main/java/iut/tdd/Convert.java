package iut.tdd;


import java.util.HashMap;

public class Convert {
	
		static HashMap<String, String> numToText = new HashMap<String, String>(){
	    	{
	    		put("0","z�ro");
	    		put("1","un");
	    		put("2","deux");
	    		put("3","trois");
	    		put("4","quatre");
	    		put("5","cinq");
	    		put("6","six");
	    		put("7","sept");
	    		put("8","huit");
	    		put("9","neuf");
	    		put("10","dix");
	    		
	    		put("20","vingt");
	    		put("30","trente");
	    		put("40","quarante");
	    		put("50","cinquante");
	    		put("60","soixante");
	    		
	    		put("100","cent");
	    	}
	    };
	    
	    static HashMap<String,String> numToTextException = new HashMap<String,String>(){
	    	{
	    		put("11","onze");
	    		put("12","douze");
	    		put("13","treize");
	    		put("14","quatorze");
	    		put("15","quinze");
	    		put("16","seize");
	    		
	    		put("70","soixante-dix");
	    		put("80","quatre-vingt");
	    		put("90","quatre-vingt-dix");
	    	}
	    };
	
	public String dizaine(String input){
		int chiffre_dizaine =0;
		int reste_division = 0;
		
		if(Integer.parseInt(input)>=10){
			chiffre_dizaine = (Integer.parseInt(input))/10;
			reste_division = (Integer.parseInt(input))%10;
		}
		
		return null;
	}
	
	public static String num2text(String input) {
		
		if(Integer.parseInt(input)<10){
			return numToText.get(input);
		}else if(numToTextException.containsValue(input)){
			return numToTextException.get(input);
		}else{
			return numToText.get(input);
		}
		
	   	//return numToText.get(input);
	}
	
	
	public static String text2num(String input) {
		
		return null;
	}
}